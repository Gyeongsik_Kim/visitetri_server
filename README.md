# ETRI 방문자 관리 앱 용 서버

ETRI 방문자 관리용 앱을 운영하기 위한 서버입니다.

서버의 스펙은 다음과 같습니다.
  - Oracle Java 8 기반
   - sun HTTP 서버 라이브러리 사용
  - 모든 Java를 지원하는 운영체제에서 사용 가능

License
----

Apache2
