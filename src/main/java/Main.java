import com.sun.net.httpserver.HttpServer;
import db.DB;
import server.*;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;

/**
 * Created by blogc on 2016-08-26.
 */
public class Main {
    private final int port = 9911;
    private InetSocketAddress inetSocketAddress;
    private HttpServer httpServer;
    private DB db;

    private void initDb() {
        db = new DB();

        System.out.println("Main: 데이터베이스를 초기화하였습니다.");
    }

    private void initServer() {
        inetSocketAddress = new InetSocketAddress(port);
        try {
            httpServer = HttpServer.create(inetSocketAddress, 0);
        } catch (BindException e) {
            System.out.println("Main: 서버가 이미 가동중이거나 포트가 사용 중 입니다.");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("Main: 서버를 초기화 하는데 실패하였습니다.");
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Main: 서버를 초기화 하였습니다.");
    }

    private void setServerContext() {
        httpServer.createContext("/alive.html", new AliveHandler(db));
        httpServer.createContext("/findid.html", new FindIdHandler(db));
        httpServer.createContext("/findpw.html", new FindPwHandler(db));
        httpServer.createContext("/login.html", new LoginHandler(db));
        httpServer.createContext("/register.html", new RegisterHandler(db));
        httpServer.createContext("/messenger.html", new MessengerHandler(db));

        System.out.println("Main: 서버를 세팅하였습니다.");
    }

    private void serverStart() {
        httpServer.start();
        System.out.println("Main: 서버를 시작합니다.");
    }

    public Main() {
        initDb();
        initServer();
        setServerContext();

        serverStart();
    }

    public static void main(String[] args) {
        new Main();
    }
}
