package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by miffle-laptop on 2016-08-08.
 */
public class DB {
    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_URL = "jdbc:mysql://192.168.188.130:3306/visit_etri";

    private final String USERNAME = "root";
    private final String PASSWORD = "visit_etri";

    private static Connection connection;

    public static synchronized Connection getConnection() {
        if (connection == null) {
            return null;
        }
        return connection;
    }

    public DB() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
            System.out.println("DB: 데이터베이스가 연결되었습니다.");
        } catch (ClassNotFoundException e) {
            System.out.println("DB: JDBC 드라이버가 존재하지 않습니다.");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("DB: 알 수 없는 데이터베이스 오류가 발생하였습니다.");
            System.out.println("DB: 서버를 종료합니다.");
            e.printStackTrace();
            System.exit(0);
        }
    }
}
