package db.dbhandler;

import java.sql.SQLException;

/**
 * Created by blogc on 2016-08-22.
 */
public class DBRegister extends CommonDB {

    public boolean getRegister(String name, String id, String password, String phone, String birthday, boolean isVisiter) {
        int visiter = 0;

        if(isVisiter == false)
            visiter = 1;

        if (name == null || id == null || password == null || phone == null || birthday == null) {
            System.out.println("DBRegister: 회원가입하는데 필요한 정보가 부적절합니다.\nDBRegister: 작업을 종료합니다.");
            return false;
        }

        String sqlCommand = "insert user VALUE(\"" + name + "\", \"" + id + "\", \"" + password + "\", \"" + phone + "\",\"" + birthday + "\"," + visiter + ")";

        try {
            statement.executeUpdate(sqlCommand);
        } catch (SQLException e) {
            System.out.println("DBRegister: 회원가입을 실패하였습니다.");
            e.printStackTrace();
            return false;
        }

        System.out.println("DBRegister: 회원가입을 성공하였습니다.");
        return true;
    }
}
