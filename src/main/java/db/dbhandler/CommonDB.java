package db.dbhandler;

import db.DB;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by blogc on 2016-08-22.
 */
public class CommonDB {
    protected ResultSet resultSet;
    protected Statement statement;

    public CommonDB() {
        try {
            this.statement = DB.getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected JSONObject executeAndGetResult(String sqlCommand) throws SQLException {
        JSONObject result = new JSONObject();

        this.resultSet = statement.executeQuery(sqlCommand);

        ResultSetMetaData metaData = resultSet.getMetaData();

        while(resultSet.next()) {
            for(int index = 0 ; index < metaData.getColumnCount(); index++) {
                result.put(metaData.getColumnName(index+1), resultSet.getString(metaData.getColumnName(index+1)));
            }
        }
        return result;
    }

}
