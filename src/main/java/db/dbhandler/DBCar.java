package db.dbhandler;

import org.json.simple.JSONObject;
import server.WorkCodes;

import java.sql.SQLException;

/**
 * Created by blogc on 2016-08-23.
 */
public class DBCar extends CommonDB {

    public JSONObject getCar(String id) throws SQLException {
        String sqlCommand = "select " + WorkCodes.CAR_ONE + ","  + WorkCodes.CAR_TWO + "," +  WorkCodes.CAR_THREE + "from" + WorkCodes.CAR_TABLE;

        JSONObject result = executeAndGetResult(sqlCommand);

        result.put(WorkCodes.COMM_STATUS, "0");

        return result;
    }

    public boolean addCar(String id, String carNumber, int order) {
        String carColumn;

        if(order > 3 || order < -1) {
            return false;
        }

        switch(order) {
            case 1:
                carColumn = WorkCodes.CAR_ONE;
                break;
            case 2:
                carColumn = WorkCodes.CAR_TWO;
                break;
            case 3:
                carColumn = WorkCodes.CAR_THREE;
                break;
            default:
                return false;
        }

        String sqlCommand = "insert into " + WorkCodes.CAR_TABLE + "(" + carColumn + ")" + " value(\"" + carNumber + "\")";

        try {
            statement.executeUpdate(sqlCommand);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
