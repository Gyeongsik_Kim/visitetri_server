package db;

import java.util.Vector;
import java.util.stream.Stream;

/**
 * Created by blogc on 2016-08-23.
 */
public class QueryMaker {

    public static String insertValue(String table, String... querys) {
        String querySentence = "insert " + table + " values " + makeQuerySentence(querys);

        return querySentence;
    }

    public static String makeQuerySentence(String... querys) {
        String querySentence = "(";

        for(String query : querys) {
            try {
                querySentence += (" " + Integer.parseInt(query));
            } catch (NumberFormatException e) {
                querySentence += ("\"" + query + "\"");
            }

            if (!(querys[querys.length-1].equals(query))) {
                querySentence += ",";
            }
        }

        querySentence += ")";

        return querySentence;
    }
}
