package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import org.json.simple.JSONObject;

import java.io.IOException;

/**
 * Created by miffle-laptop on 2016-08-08.
 */
public class AliveHandler extends PreHandler implements HttpHandler {

    public AliveHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();

        System.out.println("AliveHandler: AliveHandler가 작동하였습니다.");

        JSONObject result = new JSONObject();
        result.put("status", "1");
        writeClient(result.toJSONString());

        closeOutputStream();
        closeReader();
        closeHttpExcahnge();
    }
}
