package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import db.dbhandler.DBRegister;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

/**
 * Created by blogc on 2016-08-21.
 */
public class RegisterHandler extends PreHandler implements HttpHandler {

    public RegisterHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();

        JSONObject readed = null;

        try {
            readed = (JSONObject) new JSONParser().parse(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println("RegisterHandler: 알 수 없는 데이터를 받아왔습니다.\nRegisterHandler: 작업을 종료합니다.");
            e.printStackTrace();
            return;
        }

        String memberName = readed.get(WorkCodes.MEMBER_NAME).toString();
        String memberPassword = readed.get(WorkCodes.MEMBER_PASSWORD).toString();
        String memberBirthday = readed.get(WorkCodes.MEMBER_BIRTHDAY).toString();
        String memberPhone = readed.get(WorkCodes.MEMBER_PHONE).toString();
        String memberId = readed.get(WorkCodes.MEMBER_ID).toString();

        if (memberName == null || memberId == null || memberPassword == null || memberPhone == null) {
            System.out.println("RegisterHandler: 회원가입하는데 필요한 정보가 부족합니다.");
            return;
        }

        JSONObject result = new JSONObject();

        if(new DBRegister().getRegister(memberName, memberId, memberPassword, memberBirthday, memberPhone, true)) {
            result.put(WorkCodes.COMM_STATUS, "1");
            System.out.println("Register: " + memberName + " 님이 회원가입을 완료하였습니다.");
        } else {
            result.put(WorkCodes.COMM_STATUS, "0");
            System.out.println("Register: " + memberName + " 님이 회원가입을 실패하였습니다.");
        }

        if (result.get(WorkCodes.COMM_STATUS).toString().equals("0")) {
            System.out.println("RegisterHandler: 회원가입에 성공하였습니다.");
            writeClient(result.toJSONString());
        } else {
            System.out.println("RegisterHandler: 회원가입에 실패하였습니다.");
        }

        close();
    }
}
