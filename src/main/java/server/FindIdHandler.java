package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import db.dbhandler.DBFindId;
import db.dbhandler.DBFindPw;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by blogc on 2016-08-22.
 */
public class FindIdHandler extends PreHandler implements HttpHandler {

    public FindIdHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();

        JSONObject readed = null;

        try {
            readed = (JSONObject) new JSONParser().parse(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println("FindIdHandler: 알 수 없는 데이터를 받아왔습니다.\nFindIdHandler: 작업을 종료합니다.");
            e.printStackTrace();
            close();
            return;
        }


        String memberName = readed.get(WorkCodes.MEMBER_NAME).toString();
        String memberPhone = readed.get(WorkCodes.MEMBER_PHONE).toString();

        if (memberName == null || memberPhone == null) {
            System.out.println("FindIdHandler: 조회에 필요한 정보가 부족합니다.\nFindIdHandler: 작업을 종료합니다.");
            close();
            return;
        }

        JSONObject payload = new JSONObject();

        try {
            payload = new DBFindId().getId(memberName, memberPhone);
        } catch (SQLException e) {
            System.out.println("FindIdHandler: 존재하지 않는 유저입니다.");
            payload.put(WorkCodes.COMM_STATUS, "1");
        }

        writeClient(payload.toJSONString());

        close();
    }
}
