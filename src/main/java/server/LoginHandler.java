package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import db.dbhandler.DBLogin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by blogc on 2016-08-19.
 */
public class LoginHandler extends PreHandler implements HttpHandler {

    public LoginHandler(DB db) {
        super(db);
    }

    private boolean login(String id, String password) {
        JSONObject result = null;
        try {
            result = new DBLogin().getLogin(id, password);
        } catch (SQLException e) {
            System.out.println("LoginHandler: 로그인에 실패하였습니다.\nLoginHandler: 작업을 종료합니다.");
            e.printStackTrace();
            return true;
        }

        if (result == null || result.equals("{}")) {
            System.out.println("LoginHandler: 로그인에 실패하였습니다.\nLoginHandler: 작업을 종료합니다.");
            return true;
        } else {
            if (result.get("id").equals(id) && result.get("password").equals(password)) {
                return false;
            }
        }

        System.out.println("LoginHandler: 로그인에 실패하였습니다.\nLoginHandler: 작업을 종료합니다.");
        return true;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();


        JSONObject infos = null;

        try {
            infos = (JSONObject)new JSONParser().parse(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println("LoginHandler: 로그인에 필요한 정보를 받지 못하였습니다. 1\nLoginHandler: 작업을 종료합니다.");
            e.printStackTrace();
            close();
            return;
        }

        String id = infos.get(WorkCodes.MEMBER_ID).toString();
        String pw = infos.get(WorkCodes.MEMBER_PASSWORD).toString();

        if (id == null || pw == null) {
            System.out.println("LoginHandler: 로그인에 필요한 정보를 받지 못하였습니다. 2\nLoginHandler: 작업을 종료합니다.");
            close();
            return;
        }

        JSONObject payload = new JSONObject();

        if (login(id, pw) == false) {
            payload.put(WorkCodes.COMM_COOKIE, id.hashCode());
        }

        if (payload.get(WorkCodes.COMM_STATUS).toString().equals("0")) {
            writeClient(payload.toJSONString());
            System.out.println("LoginHandler: 다음과 같은 정보를 전송하였습니다.\nLoginHandler: " + payload.toJSONString());
        } else {
            System.out.println("LoginHandler: 로그인에 필요한 정보를 받지 못하였습니다.\nLoginHandler: 작업을 종료합니다.");
            close();
            return;
        }

        close();
    }
}
