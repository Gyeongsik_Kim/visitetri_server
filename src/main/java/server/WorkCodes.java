package server;

/**
 * Created by blogc on 2016-08-19.
 */
public class WorkCodes {
    public static final String CAR_TABLE = "usersCar";
    public static final String CAR_ONE = "car1";
    public static final String CAR_TWO = "car2";
    public static final String CAR_THREE = "car3";

    public static final String MEMBER_NAME = "member_name";
    public static final String MEMBER_PASSWORD = "member_password";

    public static final String MEMBER_PHONE = "member_phone";
    public static final String MEMBER_BIRTHDAY = "member_birthday";

    public static final String MEMBER_CAR = "member_car";
    public static final String MEMBER_ID = "member_id";

    public static final String RESERVE_NAME = "reserve_name";
    public static final String RESERVE_PLACE = "reserve_place";
    public static final String RESERVE_TIME = "reserve_time";
    public static final String RESERVE_DATE = "reserve_date";

    public static final String COMM_MESSAGE = "comm_message";
    public static final String COMM_MESSAGE_SEND = "comm_message_send";
    public static final String COMM_MESSAGE_RECEIVE = "comm_message_receive";
    public static final String COMM_MESSAGE_SENDER = "comm_message_sender";
    public static final String COMM_MESSAGE_RECEIVER = "comm_message_receiver";
    public static final String COMM_MESSAGE_TYPE = "comm_message_type";

    public static final String COMM_ADD = "comm_add";
    public static final String COMM_DELETE = "comm_delete";
    public static final String COMM_STATUS = "comm_status";
    public static final String COMM_COOKIE = "comm_cookie";
    public static final String COMM_URL = "comm_url";

}

