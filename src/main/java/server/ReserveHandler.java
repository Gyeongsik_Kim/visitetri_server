package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;

import java.io.IOException;

/**
 * Created by blogc on 2016-08-23.
 */
public class ReserveHandler extends PreHandler implements HttpHandler {

    public ReserveHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);

        openInputReader();
        openOutputStream();


    }
}
