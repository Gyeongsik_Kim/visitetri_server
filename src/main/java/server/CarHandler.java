package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

/**
 * Created by blogc on 2016-08-23.
 */
public class CarHandler extends PreHandler implements HttpHandler{

    public CarHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();

        JSONObject readed = null;

        try {
            readed = (JSONObject) new JSONParser().parse(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println("CarHandler: 알 수 없는 데이터를 받아왔습니다.\nCarHandler: 작업을 종료합니다.");
            e.printStackTrace();
            return;
        }

        String commStatus = readed.get(WorkCodes.COMM_STATUS).toString();

        if (commStatus.equals(WorkCodes.COMM_ADD)) {

        } else if (commStatus.equals(WorkCodes.COMM_DELETE)) {

        } else {
            System.out.println("CarHandler: 알 수 없는 커맨드입니다.\nCarHandler: 작업을 종료합니다.");
            close();
            return;
        }

        close();
    }
}
