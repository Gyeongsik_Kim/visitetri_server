package server;

import com.sun.net.httpserver.HttpExchange;
import db.DB;

import java.io.*;

/**
 * Created by miffle-laptop on 2016-08-08.
 */
public class PreHandler {
    protected BufferedReader bufferedReader;
    protected OutputStream outputStream;
    protected HttpExchange httpExchange;
    protected DB db;

    public PreHandler(DB db) {
        this.db = db;
    }

    protected void setHttpExchange(HttpExchange httpExchange) {
        this.httpExchange = httpExchange;
    }

    protected void openInputReader() throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody(), "utf-8"));
    }

    protected void closeReader() throws IOException {
        bufferedReader.close();
    }

    protected void openOutputStream() throws IOException {
        outputStream = httpExchange.getResponseBody();
    }

    protected void closeOutputStream() throws IOException {
        outputStream.close();
    }

    protected void writeClient(String message) throws IOException {
        httpExchange.sendResponseHeaders(200, message.getBytes().length);
        outputStream.write(message.getBytes());
    }

    protected void closeHttpExcahnge() {
        if (httpExchange != null) {
            this.httpExchange.close();
        }
    }

    protected void close() throws IOException {
        closeOutputStream();
        closeReader();
        closeHttpExcahnge();
    }
}
