package server.message;

import java.util.List;

/**
 * Created by blogc on 2016-08-25.
 */
public class MessageQueue {

    private String receiver;
    private String message;
    private String sender;

    public MessageQueue(String sender, String receiver, String message)
    {
        this.receiver = receiver;
        this.sender = sender;
        this.message = message;
    }

    public synchronized String getSender() {
        return sender;
    }

    public synchronized String getReceiver() {
        return receiver;
    }

    public synchronized String getMessage() {
        return message;
    }

    public synchronized void setSender(String user) {
        this.sender = user;
    }

    public synchronized void setReceiver(String user) {
        this.receiver = user;
    }

    public synchronized void setMessage(String message) {
        this.message = message;
    }

}
