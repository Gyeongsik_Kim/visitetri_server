package server.message;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by blogc on 2016-08-25.
 */
public class MessageService {
    private static LinkedList<MessageQueue> messageQueueList = new LinkedList<>();

    public static synchronized int getMessageQueueListSize() {
        if (messageQueueList != null) {
            return messageQueueList.size();
        }

        return 0;
    }

    public static synchronized LinkedList<MessageQueue> getMessageQueueList() {
        return messageQueueList;
    }

    public static synchronized void addMessageQueue(MessageQueue messageQueue) {
        messageQueueList.add(messageQueue);
    }

    public static synchronized MessageQueue getMessageQueue(int index) {
        return messageQueueList.get(index);
    }
}
