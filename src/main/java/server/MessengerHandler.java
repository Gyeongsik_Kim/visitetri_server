package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import server.message.MessageQueue;
import server.message.MessageService;
import util.JSON;

import java.io.IOException;

/**
 * Created by blogc on 2016-08-25.
 */
public class MessengerHandler extends PreHandler implements HttpHandler {
    private final String TAG = "MessageHandler";

    public MessengerHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openOutputStream();
        openInputReader();

        JSONObject result;

        try {
            result = JSON.stringToJSON(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println(TAG + " : 비정상적인 데이터를 받았습니다.");
            e.printStackTrace();
            return;
        }

        try {

            String commandType = result.get(WorkCodes.COMM_MESSAGE_TYPE).toString();
            String user = result.get(WorkCodes.MEMBER_ID).toString();

            if(commandType.equals(WorkCodes.COMM_MESSAGE_RECEIVE)) {
                JSONObject payload = null;

                for(MessageQueue queue : MessageService.getMessageQueueList()) {
                    if (user.equals(queue.getReceiver())) {
                        payload = new JSONObject();
                        payload.put(WorkCodes.COMM_MESSAGE_SENDER, queue.getSender());
                        payload.put(WorkCodes.COMM_MESSAGE, queue.getMessage());
                        System.out.println(TAG + ": " + queue.getSender() + "님이 " +  queue.getReceiver() + "님에게 메시지 \"" + queue.getMessage() + "\"를 받았습니다.");
                        MessageService.getMessageQueueList().remove(MessageService.getMessageQueueList().indexOf(queue));
                    }
                }

                if (payload != null) {
                    writeClient(payload.toJSONString());
                }
            } else if (commandType.equals(WorkCodes.COMM_MESSAGE_SEND)) {
                String receiver = result.get(WorkCodes.COMM_MESSAGE_RECEIVER).toString();
                String sender = result.get(WorkCodes.COMM_MESSAGE_SENDER).toString();
                String message = result.get(WorkCodes.COMM_MESSAGE).toString();

                System.out.println(TAG + ": " + user + "님이 " +  receiver + "님에게 메시지 \"" + message + "\"를 전송하셨습니다.");
                MessageService.addMessageQueue(new MessageQueue(sender, receiver, message));
                JSONObject payload = new JSONObject();
                payload.put(WorkCodes.COMM_STATUS, "1");
                writeClient(payload.toJSONString());
            }
        } catch (NullPointerException e) {
            System.out.println(TAG + " : 비정상적인 데이터를 받았습니다.");
            e.printStackTrace();
        }

        try {
            close();
        } catch (IOException e) {
            JSONObject payload = new JSONObject();
            payload.put(WorkCodes.COMM_STATUS, "0");
            writeClient(payload.toJSONString());
            close();
        }
    }
}
