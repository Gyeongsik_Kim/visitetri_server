package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DB;
import db.dbhandler.DBFindPw;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by blogc on 2016-08-19.
 */
public class FindPwHandler extends PreHandler implements HttpHandler {

    public FindPwHandler(DB db) {
        super(db);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        setHttpExchange(httpExchange);
        openInputReader();
        openOutputStream();

        JSONObject payload = null;

        try {
            payload = (JSONObject) new JSONParser().parse(bufferedReader.readLine());
        } catch (ParseException e) {
            System.out.println("FindPwHandler: 올바르지 않은 정보를 받았습니다.\nFindPwHandler: 작업을 종료합니다.");
            e.printStackTrace();
            close();
            return;
        }

        String memberName = payload.get(WorkCodes.MEMBER_NAME).toString();
        String memberPhone = payload.get(WorkCodes.MEMBER_NAME).toString();
        String memberBirthday = payload.get(WorkCodes.MEMBER_NAME).toString();

        if (memberName == null || memberPhone == null || memberBirthday == null) {
            System.out.println("FindPwHandler: 비밀번호를 찾는데 필요한 정보가 부족합니다.\nFindPwHandler: 작업을 종료합니다");
            close();
            return;
        }

        JSONObject result = null;

        try {
            result = new DBFindPw().getPassword(memberName, memberPhone, memberBirthday);
        } catch (SQLException e) {
            System.out.println("FindPwHandler: 비밀번호 찾기를 실패하였습니다.\nFindPwHandler: 작업을 종료합니다.");
            e.printStackTrace();
            close();
            return;
        }

        if (result.get(WorkCodes.COMM_STATUS).toString().equals("0")) {
            writeClient(result.toJSONString());
        } else {
            System.out.println("FindPwHandler: 비밀번호 찾기에 실패하였습니다.");
        }

        close();
    }
}
