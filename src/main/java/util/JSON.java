package util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by blogc on 2016-08-25.
 */
public class JSON {

    public static JSONObject stringToJSON(String jsonString) throws ParseException {
        return (JSONObject) new JSONParser().parse(jsonString);
    }
}
